import numpy as np
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegression
import pandas as pd
import matplotlib.pyplot as plt
import numpy
from sklearn.metrics import precision_recall_curve, auc
from sklearn.model_selection import KFold


class ClassifierEvaluation:

	def __init__(self):
		pass

	def plot_learning_curve(self, model, X, y, scorer, sizes=np.linspace(0.1, 1, 5), cv=None, n_jobs=5, ylim=None, title="Xval. learning curve"):
		''' Plot learning curve for model on data '''

		df = pd.DataFrame()
		train_sizes, train_scores, test_scores = learning_curve(model, X, y, cv=cv, n_jobs=n_jobs, train_sizes=sizes)
		df['sizes_p'] = sizes
		df['sizes_n'] = train_sizes
		df['train_mean'] = 1 - np.mean(train_scores, axis=1)
		df['train_std'] = np.std(train_scores, axis=1)
		df['test_mean'] = 1 - np.mean(test_scores, axis=1)
		df['test_std'] = np.std(test_scores, axis=1)

		fig = plt.figure()
		ax = fig.add_subplot(111)
		ax.set_title(title)
		if ylim is not None:
			ax.set_ylim(*ylim)
		ax.set_xlabel("Size of training set")
		ax.set_ylabel("Error (1-score)")
		ax.grid()
		ax.fill_between(sizes, df.train_mean - df.train_std, df.train_mean + df.train_std, alpha=0.1, color="r")
		ax.fill_between(sizes, df.test_mean - df.test_std, df.test_mean + df.test_std, alpha=0.1, color="g")
		ax.plot(sizes, df.train_mean, 'o-', color="r", label="Training")
		ax.plot(sizes, df.test_mean, 'o-', color="g", label="Test")
		ax.legend(loc="best")
		fig.show()
		return df, fig

	# Print the learning curves
	def plot_learning_curves_for_tweets(self, X, y, model):#best_hyperparameter):

		# Convert a sparce matrxi to dense
		X = X.toarray()

		cv = ShuffleSplit(n_splits=5, test_size=0.2, random_state=0)
		# estimator = LogisticRegression(C=best_hyperparameter)
		# self.plot_learning_curve(estimator, X, y, scorer='None', cv=cv, n_jobs=2, ylim=(0.0, 0.5), title="Learning Curves (Logistic Regression)")
		self.plot_learning_curve(model, X, y, scorer='None', cv=cv, n_jobs=2, ylim=(0.0, 0.5), title="Learning Curves (Logistic Regression)")

		plt.show()

	# Print the precision - recall curves
	def plot_precision_recall_curves(self, X, y, best_hyperparameter):

		FOLDS = 5

		fig = plt.figure()
		ax = fig.add_subplot(111)

		k_fold = KFold(n_splits=FOLDS, shuffle=True, random_state=12345)
		predictor = LogisticRegression(C=best_hyperparameter)

		y_real = []
		y_proba = []

		for i, (train_index, test_index) in enumerate(k_fold.split(X)):
			Xtrain, Xtest = X[train_index], X[test_index]
			ytrain, ytest = y[train_index], y[test_index]
			predictor.fit(Xtrain, ytrain)
			pred_proba = predictor.predict(Xtest)
			print(ytest)
			print(pred_proba)
			precision, recall, _ = precision_recall_curve(np.array(ytest), np.array(pred_proba))
			lab = 'Fold %d AUC=%.4f' % (i + 1, auc(recall, precision))
			ax.step(recall, precision, label=lab)
			y_real.append(ytest)
			y_proba.append(pred_proba[:, 1])

		y_real = numpy.concatenate(y_real)
		y_proba = numpy.concatenate(y_proba)
		precision, recall, _ = precision_recall_curve(y_real, y_proba)
		lab = 'Overall AUC=%.4f' % (auc(recall, precision))
		ax.step(recall, precision, label=lab, lw=2, color='black')
		ax.set_xlabel('Recall')
		ax.set_ylabel('Precision')
		ax.legend(loc='lower left', fontsize='small')

		fig.savefig('PR_curve')
		fig.show()