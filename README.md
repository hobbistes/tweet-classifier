## Text Engineering and Analytics - Project 2
### Tweet Sentiment Classifier

#### Project Members
* Raikakou Evgenia
* Tsaprailis Konstantinos
* Choumos George

##### Installing Prerequisites
To install the requirements you can run `pip3 install -r requirements.txt`

They are in particular:
* numpy
* scipy
* sklearn
* matplotlib

* There is an one-time need to download 'wordnet' from nltk. This doesn't need to happen from inside the program. You can just do it through a python shell.

```
import nltk
nltk.download('wordnet')
```

