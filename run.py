"""
DONE:

TODO:
	 - Add arguments for:
	   * preprocess_data method (Count Vectorizer / tf-idf vectorizer)
	   * n-gram parameter on the previous
	   * train/devtest/test percentages
	   * Classifier (select one/run for all)
	 - Plot the learning curves. The learning data are already available.
	 - k-Fold validation in the main model fit.
	 - Caclulate a regularization parameter.
	 - Word embeddings!
	 - Linear Interpolation
"""

from data_loader import DataLoader
from tweet_classifier import TweetClassifier
from classifier_evaluation import ClassifierEvaluation
from scipy.sparse import vstack
import numpy as np


def main():

	# Training dataset sizes for learning curves generation
	learning_sizes = [100, 1000, 2000, 5000, 10000, 20000, 50000, 71980]
	testload = DataLoader("./sentiment_sample_100000.csv")
	testload.read_data_from_csv()

	# Get the data subsets
	train, devtest, test = testload.split_dataset()
	# "1" corresponds to the tf-idf vectorizer which yields better results.
	# For the CountVectorizer --> "0"
	classifier = TweetClassifier(1)
	transformed_train, sentiments_train = classifier.preprocess_data(train)
	transformed_devtest,sentiments_devtest = classifier.transform_data(devtest)
	transformed_test,sentiments_test = classifier.transform_data(test)

	# Train the logistic classifier
	classifier.logistic_classifier(transformed_train,sentiments_train)

	# Predict using the logistic classifier
	predictions = classifier.logistic_classifier_predict(transformed_devtest)
	print('Log Reg Accuracy')
	classifier.prediction_score(predictions, sentiments_devtest)

	# Learning curves for Log Regression
	# We'll need to concatenate the training and test sparce matrices as
	# it splits them on its own.
	x = vstack([transformed_train,transformed_devtest])
	y = np.concatenate((sentiments_train,sentiments_devtest),axis=0)
	# Learning data for Logistic Regression
	classifier.generate_learning_curves(classifier.logreg,learning_sizes,x,y)

	# Train the naive bayes classifier
	classifier.naive_bayes_classifier(transformed_train,sentiments_train)

	# Predict using the naive bayes classifier
	predictions = classifier.naive_bayes_classifier_predict(transformed_devtest)
	print('Naive Bayes Accuracy')
	classifier.prediction_score(predictions, sentiments_devtest)
	# Learning data for NB
	classifier.generate_learning_curves(classifier.naive,learning_sizes, x,y)

	# Train the SVM classifier
	classifier.svm_classifier(transformed_train,sentiments_train)

	# Predict using the SVM classifier
	predictions = classifier.svm_classifier_predict(transformed_devtest)
	print('SVM Accuracy')
	classifier.prediction_score(predictions, sentiments_devtest)
	# Learning data for SVM
	classifier.generate_learning_curves(classifier.svm,learning_sizes, x,y)

	# Train the KNN classifier
	# kNN part - DISABLED for now
	if sentiments_train.shape[0] > 0:
		print("kNN Disabled! :) Try again later")

	else:
		# Predict using the KNN classifier
		classifier.knn_classifier(transformed_train,sentiments_train)
		predictions = classifier.knn_classifier_predict(transformed_devtest)
		print('KNN Accuracy')
		classifier.prediction_score(predictions, sentiments_devtest)
		# Learning data for kNN
		classifier.generate_learning_curves(classifier.knn,learning_sizes, x,y)


if __name__ == '__main__':
	main()