"""
	DONE:
		- Tokenize tweets
		- Remove stopwords - done through vectorizer arguments
		- Do we need to keep the tweets and sentiments saved? Can we just
		  compute and return them, thus saving a good amount of memory?
			Not relevant anymore
		- Things we want to consider:
		  * We may want to set the n_jobs argument to -1 for kNN) so that all the
			cpu cores are utilized. Abandoned as the performance improvement was
			not important on a personal PC.


	TODO:
		- Stemming:
			Implemented through the "tokenizer" argument on the vectorizers.
			See also the extra LemmaTokenizer class at the bottom. It led to
			and ~1% gain on performance. Disabled for now as it introduces higher
			training times.

		- Things we want to tune:
		  * C parameter of the LogRegression. - DONE
		  * k value for k-NN.
		  * vectorizer method (0 for Count, 1 for TfIdf). Though we might
			eventually hardcode it to 1.
		  * ngram_range (on count and tfidf vectorizers)
		  * alpha parameter for BernoulliNB. Default is 1.0

		- We could mention that we selected the Linear kernel for SVM (LinearSVC)
		  as the default one's (SVC) complexity is more than quadratic, thus
		  it would become computationaly infeasible for size > 10.000.
"""

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, roc_auc_score, roc_curve
from sklearn.metrics import precision_recall_curve, auc
from sklearn import svm
from sklearn.model_selection import learning_curve
from sklearn.model_selection import GridSearchCV
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize 
import numpy as np
from datetime import datetime
import pickle
import pandas as pd
import os
import matplotlib.pyplot as plt

class TweetClassifier(object):

	method = None # Select between Count (0) and Tfidf (1) Vectorizers
	tweets = []   # without the sentiments
	sentiments = []
	vectorizer = None
	logreg = None
	naive = None
	knn = None
	svm = None

	def __init__(self,method=0):
		self.method = method
		self.model_path = '../models/model_' + datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '/'
		# if not os.path.exists(os.path.dirname(self.model_path)):
		# 	os.makedirs(os.path.dirname(self.model_path))

	def _save_object(self, obj, path):
		print(path)
		with open(path, 'wb') as f:
			pickle.dump(obj, f)

	def preprocess_data(self,data):
		for tweet_sentiment in data:
			self.tweets.append(tweet_sentiment[1])
			self.sentiments.append(tweet_sentiment[0])

		if self.method == 0:
			self.vectorizer = CountVectorizer(ngram_range=(1,3), stop_words='english')#,tokenizer=LemmaTokenizer())
		else:
			self.vectorizer = TfidfVectorizer(ngram_range=(1,3), stop_words='english')#,tokenizer=LemmaTokenizer())
		transformed_tweets = self.vectorizer.fit_transform(self.tweets)
		vectorizer_file = os.path.join(self.model_path, 'vectorizer.pkl')
		# self._save_object(self.vectorizer, vectorizer_file)
		return transformed_tweets, np.array(self.sentiments)

	def transform_data(self, data):
		tweets = []
		sentiments = []
		for tweet_sentiment in data:
			tweets.append(tweet_sentiment[1])
			sentiments.append(tweet_sentiment[0])

		return self.vectorizer.transform(tweets), np.array(sentiments)

	def logistic_classifier(self,trainX,trainY):
		tuned_parameters = [{'C': [1e-2, 1e-1, 1, 1e1, 1e2, 1e3]}]
		score = 'accuracy'
		self.logreg = GridSearchCV(LogisticRegression(), tuned_parameters, cv=5,
					   scoring=score)
		# self.logreg = LogisticRegression(C=1e5)
		self.logreg.fit(trainX,trainY)
		print('Best params found:')
		print(self.logreg.best_params_)
		logregclf_file = os.path.join(self.model_path, 'logregclf.pkl')
		#self._save_object(self.logreg, logregclf_file)

	def logistic_classifier_predict(self,testX):
		log_preds = self.logreg.predict(testX)
		return log_preds

	def naive_bayes_classifier(self,trainX,trainY):
		tuned_parameters = [{'alpha': [1e-3, 1e-2, 1e-1, 1, 1e1, 1e2]}]
		score = 'accuracy'
		# self.naive = BernoulliNB()
		self.naive = GridSearchCV(BernoulliNB(), tuned_parameters, cv=5,
					   scoring=score)
		self.naive.fit(trainX,trainY)
		print('Best params found:')
		print(self.naive.best_params_)
		naiveclf_file = os.path.join(self.model_path, 'naiveclf.pkl')
		#self._save_object(self.naive, naiveclf_file)

	def naive_bayes_classifier_predict(self,testX):
		naive_preds = self.naive.predict(testX)
		return naive_preds

	def knn_classifier(self,trainX,trainY):
		tuned_parameters = [{'n_neighbors': [3,5,7],
							 'weights': ['uniform','distance']}]
		score = 'accuracy'
		self.knn = GridSearchCV(KNeighborsClassifier(), tuned_parameters, cv=5,
					   scoring=score)
		#self.knn = KNeighborsClassifier(n_neighbors=3, weights='distance')
		self.knn.fit(trainX,trainY)
		print('Best params found:')
		print(self.knn.best_params_)
		knnclf_file = os.path.join(self.model_path, 'knnclf.pkl')
		#self._save_object(self.knn, knnclf_file)

	def knn_classifier_predict(self,testX):
		knn_preds = self.knn.predict(testX)
		return knn_preds

	def svm_classifier(self,trainX,trainY):
		self.svm = svm.LinearSVC()
		self.svm.fit(trainX,trainY)
		svmclf_file = os.path.join(self.model_path, 'svmclf.pkl')
		# self._save_object(self.svm, svmclf_file)

	def svm_classifier_predict(self,testX):
		svm_preds = self.svm.predict(testX)
		return svm_preds

	def prediction_score(self, predicted, actual):
		hits = 0
		for i, prediction in enumerate(predicted):
			if prediction == actual[i]:
				hits += 1

		print(hits / len(predicted))

	def calculate_f1_score(self, test_Y, preds_Y):
		# calculate f1
		return f1_score(test_Y, preds_Y, average='micro')

	def generate_learning_curves(self, classifier, sizes, X, y, ylim=(0.0, 1.2)):
		# print("Shape of X: ", X.shape)
		# print("Shape of y: ", y.shape)
		train_sizes, train_scores, valid_scores = learning_curve(classifier, X, y, train_sizes=sizes, cv=5)

		#print("Train Sizes:\n", train_sizes)
		#print("Train Scores:\n", train_scores)
		#print("Validation Scores:\n", valid_scores)

		mean_train_scores = []
		mean_valid_scores = []
		for l in train_scores:
			mean_train_scores.append(sum(l)/len(l))
		for l in valid_scores:
			mean_valid_scores.append(sum(l)/len(l))

		for i, size in enumerate(train_sizes):
			print('Size:',size, 'train: ', mean_train_scores[i], 'valid:', mean_valid_scores[i])
		title="Xval. learning curve"
		df = pd.DataFrame()
		df['sizes_p'] = sizes
		df['sizes_n'] = train_sizes
		df['train_mean'] = 1 - np.mean(mean_train_scores)
		df['train_std'] = np.std(mean_train_scores)
		df['test_mean'] = 1 - np.mean(mean_valid_scores)
		df['test_std'] = np.std(mean_valid_scores)

		fig = plt.figure()
		ax = fig.add_subplot(111)
		ax.set_title(title)
		if ylim is not None:
			ax.set_ylim(*ylim)
		ax.set_xlabel("Size of training set")
		ax.set_ylabel("Accuracy")
		ax.plot(sizes, mean_train_scores, 'o-', color="r", label="Training")
		ax.plot(sizes, mean_valid_scores, 'o-', color="g", label="Test")
		ax.legend(loc="best")
		fig.show()
		plt.show()

	# Calculate the roc curves
	def print_the_roc_curves(self, test_X, test_Y, classifier):
		logit_roc_auc = roc_auc_score(test_Y, classifier.predict(test_X))
		fpr, tpr, thresholds = roc_curve(test_Y, classifier.predict_proba(test_X)[:, 1])
		plt.figure()
		plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
		plt.plot([0, 1], [0, 1], 'r--')
		plt.xlim([0.0, 1.0])
		plt.ylim([0.0, 1.05])
		plt.xlabel('False Positive Rate')
		plt.ylabel('True Positive Rate')
		plt.title('Receiver operating characteristic')
		plt.legend(loc="lower right")
		plt.savefig('Log_ROC')
		plt.show()


class LemmaTokenizer(object):
	def __init__(self):
		self.wnl = WordNetLemmatizer()
	def __call__(self, doc):
		return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]
